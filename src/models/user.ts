import { useModel } from '@umijs/max';
import { useState, useCallback } from 'react';
import { flushSync } from 'react-dom';

export default () => {
  const [user, setUser] = useState<RES.UserListItem>();
  const decrement = useCallback((u: RES.UserListItem) => setUser(u), []);
  const { initialState, loading, refresh, setInitialState } = useModel('@@initialState');
  const userLogin = async () => {
    const userInfo = await initialState?.fetchUserInfo?.();
    if (userInfo) {
      flushSync(() => {
        setInitialState((s) => ({
          ...s,
          currentUser: userInfo,
        }));
      });
    }
  };
  return { user };
};
