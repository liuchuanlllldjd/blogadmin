// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 获取当前的用户 GET /api/currentUser */
export async function currentUser(options?: { [key: string]: any }) {
  return request<RES.UserListItem>('/user/info', {
    method: 'GET',
    ...(options || {}),
  });
}

/** 获取用户列表 GET /api/user */
export async function getUserList(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const { list, total } = await request<RES.UserList>('/user', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
  return {
    data: list,
    total,
    success: true,
  };
}

/** 新建用户 POST /api/user/addUser */
export async function addUser(data: REQ.AddUserParams, options?: { [key: string]: any }) {
  return request<RES.UserListItem>('/user/addUser', {
    method: 'POST',
    data,
    ...(options || {}),
  });
}

/** 删除用户 DELETE /api/user */
export async function removeUserApi(userId: number, options?: { [key: string]: any }) {
  return request<Record<string, any>>(`/api/user/${userId}`, {
    method: 'DELETE',
    ...(options || {}),
  });
}

/**
 *
 * @param userId
 * @param data
 * 批量删除用户
 */
export async function deleteUserListApi(ids: number[]) {
  return request<Record<string, any>>(`/api/user`, {
    method: 'DELETE',
    data: {
      ids,
    },
  });
}

/**
 *
 * @param userId
 * @param data
 * 更新用户
 */
export async function updateUserApi(userId: number, data: REQ.UpdateUserParams) {
  return request<Record<string, any>>(`/api/user/${userId}`, {
    method: 'PATCH',
    data,
  });
}
