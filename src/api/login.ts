// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 发送验证码 POST /api/login/captcha */
export async function getFakeCaptcha(
  params: {
    // query
    /** 手机号 */
    phone?: string;
  },
  options?: { [key: string]: any },
) {
  return request<RES.FakeCaptcha>('/login/captcha', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 登录接口 POST /api/auth/login */
export async function login(body: REQ.LoginParams) {
  return request<RES.LoginResult>('/auth/login', {
    method: 'POST',
    data: body,
  });
}

/** 注册接口 POST /api/user/register */
export async function registerApi(body: REQ.LoginParams) {
  return request<RES.LoginResult>('/user/register', {
    method: 'POST',
    data: body,
  });
}
