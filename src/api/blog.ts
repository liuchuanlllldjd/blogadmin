import { request } from '@umijs/max';

/** 获取用户列表 GET /api/blog */
export async function getBlogList(params: REQ.TGetBlogList) {
  return request<RES.TBlogList>('/blog', {
    method: 'GET',
    params: {
      ...params,
    },
  });
}
