interface Response<T> {
  success: boolean;
  data: T;
  message?: string;
  errorCode?: number;
  errorMessage?: string;
}
enum Gender {
  'WOMEN' = '0',
  'MAN' = '1',
  'UNKNOWN' = '2',
}

declare namespace RES {
  type LoginResult = {
    token: string;
  };

  type UserListItem = {
    userId: number;
    avatar: string;
    username: string;
    gender: Gender;
    nickname?: string;
    email?: string;
    createdAt?: string;
    phone?: string;
    access: string;
  };

  type UserList = {
    list?: UserListItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  };

  type FakeCaptcha = {
    code?: number;
    status?: string;
  };

  type ErrorResponse = {
    /** 业务约定的错误码 */
    errorCode: string;
    /** 业务上的错误信息 */
    errorMessage?: string;
    /** 业务上的请求是否成功 */
    success?: boolean;
  };

  type NoticeIconList = {
    data?: NoticeIconItem[];
    /** 列表的内容总数 */
    total?: number;
    success?: boolean;
  };

  type NoticeIconItemType = 'notification' | 'message' | 'event';

  type NoticeIconItem = {
    id?: string;
    extra?: string;
    key?: string;
    read?: boolean;
    avatar?: string;
    title?: string;
    status?: string;
    datetime?: string;
    description?: string;
    type?: NoticeIconItemType;
  };

  type TBlogItem = {
    blogId: number;
    content: string;
    title: string;
    blogType: string;
    isDraft: string;
    createdAt: string;
    updatedAt: string;
  };

  type TBlogList = {
    list: TBlogItem[];
    total: number;
    pageSize: number;
    current: number;
  };
}

declare namespace REQ {
  type PageParams = {
    current?: number;
    pageSize?: number;
    nickname?: string;
    gender?: Gender;
    time?: string;
  };

  type LoginParams = {
    username?: string;
    password?: string;
  };

  type AddUserParams = {
    nickname: string;
    phone: string;
    gender: Gender;
  };

  type UpdateUserParams = {
    nickname?: string;
    gender?: Gender;
    phone?: string;
    email?: string;
  };

  type TGetBlogList = {
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
    title?: string;
    blogType?: string;
  };
}
