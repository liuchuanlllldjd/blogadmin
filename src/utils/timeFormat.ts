import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

dayjs.extend(utc);
dayjs.extend(timezone);

export const timeToUnix = (date: string | undefined | null) => {
  if (!date) return date;
  return Math.floor(Date.parse(date) / 1000);
};

export const timeToDate = (utcTime: string | undefined | null) => {
  if (!utcTime) return utcTime;
  // 将 UTC 时间字符串转换为 Day.js 对象
  const date = dayjs.utc(utcTime);

  // 将时区设置为北京时间
  date.tz('Asia/Shanghai');

  // 格式化日期和时间为指定格式
  const formattedTime = date.format('YYYY-MM-DD HH:mm');

  return formattedTime;
};

export const getTime = (utcTime: string | undefined | null) => {
  if (!utcTime) return dayjs();
  // 将 UTC 时间字符串转换为 Day.js 对象
  const date = dayjs.utc(utcTime);

  // 将时区设置为北京时间
  date.tz('Asia/Shanghai');

  return date;
};

export function getTimeAgo(utcTime: string): string {
  const now = dayjs().tz('Asia/Shanghai');
  const date = timeToDate(utcTime);

  const secondDiff = now.diff(date, 'second');
  if (secondDiff < 60) {
    return `${secondDiff} 秒前`;
  }

  const minuteDiff = now.diff(date, 'minute');
  if (minuteDiff < 60) {
    return `${minuteDiff} 分钟前`;
  }

  const hourDiff = now.diff(date, 'hour');
  if (hourDiff < 24) {
    return `${hourDiff} 小时前`;
  }

  const dayDiff = now.diff(date, 'day');
  if (dayDiff < 7) {
    return `${dayDiff} 天前`;
  }

  const weekDiff = now.diff(date, 'week');
  if (weekDiff < 4) {
    return `${weekDiff} 周前`;
  }

  const monthDiff = now.diff(date, 'month');
  return `${monthDiff} 个月前`;
}
