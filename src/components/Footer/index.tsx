import { GithubOutlined, GitlabOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-components';
import { useIntl } from '@umijs/max';
import React from 'react';

const Footer: React.FC = () => {
  const currentYear = new Date().getFullYear();

  return (
    <DefaultFooter
      style={{
        background: 'none',
      }}
      copyright={`${currentYear} blog admin`}
      links={[
        {
          key: 'lioblog',
          title: 'lio blog',
          href: 'http://blog.lioblog.cn',
          blankTarget: true,
        },
        {
          key: 'github',
          title: <GithubOutlined />,
          href: 'https://github.com/liuchuanlllldjd',
          blankTarget: true,
        },
        {
          key: 'gitlab',
          title: <GitlabOutlined />,
          href: ' https://gitlab.com/liuchuanlllldjd',
          blankTarget: true,
        },
        {
          key: 'pro Components',
          title: 'Ant Design Components',
          href: 'https://procomponents.ant.design/',
          blankTarget: true,
        },
        {
          key: 'Ant Design pro ',
          title: 'Ant Design Pro',
          href: 'https://pro.ant.design/zh-CN/',
          blankTarget: true,
        },
      ]}
    />
  );
};

export default Footer;
