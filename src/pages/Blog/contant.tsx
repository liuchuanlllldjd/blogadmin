import type { ColumnsType } from 'antd/es/table';

import { timeToDate } from '@/utils/timeFormat';

const findName = (value: string | number, options: any[]): string => {
  return options.find((i) => i.value === value)?.label || '未定义';
};

export const blogTypeOptions = [
  { label: '前端', value: '1' },
  { label: '后端', value: '2' },
];

export const columns: ColumnsType<RES.TBlogItem> = [
  {
    fixed: 'left',
    title: '文章标题',
    dataIndex: 'title',
    ellipsis: true,
    filterSearch: true,
  },
  {
    title: '文章内容',
    dataIndex: 'content',
    ellipsis: true,
  },
  {
    title: '文章类型',
    dataIndex: 'blogType',
    sorter: (a, b) => {
      return +a.blogType - +b.blogType;
    },
    render(v) {
      return <div>{findName(v, blogTypeOptions)}</div>;
    },
    filters: [
      {
        text: '前端',
        value: '1',
      },
      {
        text: '后端',
        value: '',
      },
    ],
    onFilter: (value, record) => {
      console.log(record);

      return record.title.includes(value as string);
    },
  },
  {
    title: '草稿状态',
    dataIndex: 'isDraft',
    filters: [
      {
        text: '草稿',
        value: true,
      },
      {
        text: '已发布',
        value: false,
      },
    ],
    onFilter: (value, record) => record.isDraft === value,
    render: (isDraft) => <div>{isDraft ? '草稿' : '已发布'}</div>,
  },
  {
    title: '创建时间',
    dataIndex: 'createdAt',
    render(createdAt) {
      return <div>{timeToDate(createdAt)}</div>;
    },
  },
];
