import { Button, Card, Form, Input, Select, Table } from 'antd';
import type { TablePaginationConfig } from 'antd/es/table';
import React, { useEffect, useState } from 'react';

import { getBlogList } from '@/api/blog';
import { blogTypeOptions, columns } from './contant';

const BlogList: React.FC = () => {
  const [data, setData] = useState<RES.TBlogItem[]>();
  const [loading, setLoading] = useState(false);
  const [pagination, setPagination] = useState<TablePaginationConfig>({
    current: 1,
    pageSize: 10,
    total: 0,
    showTotal: (total) => `Total ${total} items`,
  });
  const [form] = Form.useForm();

  const fetchData = async (params?: REQ.TGetBlogList) => {
    setLoading(true);
    const { current, pageSize } = pagination;
    const { list, total } = await getBlogList({ current, pageSize, ...params });
    setPagination((pagination) => {
      return {
        ...pagination,
        total,
      };
    });
    setData(list);
    setLoading(false);
  };
  const handleTableChange = (pagination: TablePaginationConfig) => {
    setPagination((v) => ({ ...v, ...pagination }));
  };

  useEffect(() => {
    fetchData();
  }, [pagination.current, pagination.pageSize]);

  const onFinish = async (values: any) => {
    await fetchData(values);
  };

  return (
    <>
      <Card style={{ marginBottom: '20px' }}>
        <Form form={form} name="horizontal_login" layout="inline" onFinish={onFinish}>
          <Form.Item label="文章标题" name="title">
            <Input placeholder="请输入文章标题" />
          </Form.Item>
          <Form.Item label="文章类型" name="blogType">
            <Select
              style={{ width: '200px' }}
              options={blogTypeOptions}
              placeholder="请输入文章类型"
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              查询
            </Button>
          </Form.Item>
        </Form>
      </Card>
      <Card>
        <Table
          loading={loading}
          pagination={pagination}
          columns={columns}
          dataSource={data}
          onChange={handleTableChange}
          rowKey="blogId"
          sticky
        />
      </Card>
    </>
  );
};
export default BlogList;
