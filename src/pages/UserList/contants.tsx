import type { ProFormColumnsType } from '@ant-design/pro-components';

export const valueEnum = {
  0: { text: '女', status: 'woman', value: 0 },
  1: { text: '男', status: 'man', value: 0 },
  2: { text: '未知', status: 'unknown', value: 0 },
};

export type DataItem = {
  name: string;
  state: string;
};

export const columnsForm: ProFormColumnsType<REQ.AddUserParams>[] = [
  {
    title: '用户名',
    dataIndex: 'nickname',
    fieldProps: {
      placeholder: '请输入用户名',
    },
    formItemProps: {
      rules: [
        {
          required: true,
          message: '请填写用户名',
        },
      ],
    },
    width: 'md',
    colProps: {
      xs: 24,
      md: 12,
    },
  },
  {
    title: '性别',
    dataIndex: 'gender',
    valueType: 'select',
    valueEnum,
    fieldProps: {
      placeholder: '请选择性别',
    },
    formItemProps: {
      rules: [
        {
          required: true,
          message: '请选择性别',
        },
      ],
    },
    width: 'md',
    colProps: {
      xs: 24,
      md: 12,
    },
  },
  {
    title: '手机号',
    dataIndex: 'phone',
    fieldProps: {
      placeholder: '请输入手机号',
    },
    formItemProps: {
      rules: [
        {
          required: true,
          message: '请填写手机号',
        },
      ],
    },
    width: 'md',
    colProps: {
      xs: 24,
      md: 12,
    },
  },
];
