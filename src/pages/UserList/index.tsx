import { addUser, deleteUserListApi, getUserList, removeUserApi, updateUserApi } from '@/api/user';
import { PlusOutlined, LoadingOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns, ProDescriptionsItemProps } from '@ant-design/pro-components';
import {
  BetaSchemaForm,
  FooterToolbar,
  PageContainer,
  ProDescriptions,
  ProTable,
} from '@ant-design/pro-components';
import { FormattedMessage, useIntl } from '@umijs/max';
import { Button, Drawer, message, Popconfirm, Upload } from 'antd';
import React, { useRef, useState } from 'react';
import { columnsForm, valueEnum } from './contants';
import type { Dayjs } from 'dayjs';
import dayjs from 'dayjs';

/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemoveAll = async (selectedRows: RES.UserListItem[]) => {
  const ids = selectedRows.map((i) => i.userId);
  await deleteUserListApi(ids);
  message.success('删除成功');
};

/**
 *
 * @param userId
 * 删除用户
 */
const handlerRemoveUser = async (userId: number | undefined) => {
  if (!userId) return message.error('未选择用户');
  await removeUserApi(userId);
  message.success('删除成功');
};

/**
 *
 * @param user
 * 添加用户
 */
const handleAddUser = async (user: REQ.AddUserParams) => {
  try {
    await addUser(user);
    message.success('Added successfully');
    return true;
  } catch (error) {
    return false;
  }
};

/**
 *
 * @param key
 * @param newUser
 * @param oldUser
 */
const handlerUpdateUser = async (
  key: keyof RES.UserListItem,
  newUser: RES.UserListItem,
  oldUser: RES.UserListItem,
) => {
  if (oldUser[key] === newUser[key]) return;
  await updateUserApi(newUser.userId, newUser);
};

const TableList: React.FC = () => {
  const [showDetail, setShowDetail] = useState<boolean>(false);

  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<RES.UserListItem>();
  const [selectedRowsState, setSelectedRows] = useState<RES.UserListItem[]>([]);

  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<RES.UserListItem>[] = [
    {
      title: '头像',
      dataIndex: 'avatar',
      valueType: 'avatar',
      hideInSearch: true,
      editable: false,
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.updateForm.userName.nameLabel"
          defaultMessage="User name"
        />
      ),
      dataIndex: 'nickname',
      tip: 'The rule name is the unique key',
      render: (dom, entity) => {
        return (
          <a
            onClick={() => {
              setCurrentRow(entity);
              setShowDetail(true);
            }}
          >
            {dom}
          </a>
        );
      },
    },
    {
      title: '用户账号',
      dataIndex: 'username',
    },
    {
      title: '性别',
      dataIndex: 'gender',
      valueType: 'select',
      valueEnum,
    },
    {
      title: '创建日期',
      dataIndex: 'time',
      sorter: true,
      valueType: 'dateTimeRange',
      hideInTable: true,
      hideInDescriptions: true,
      editable: false,
    },
    {
      title: '创建日期',
      dataIndex: 'createdAt',
      sorter: true,
      hideInSearch: true,
      renderText: (v) => dayjs(v).format('YYYY-MM-DD HH:mm:ss'),
      editable: false,
    },
    {
      title: '手机号',
      dataIndex: 'phone',
      hideInSearch: true,
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      hideInSearch: true,
      render: (_, record) => [
        <Popconfirm
          key="removeUser"
          title="删除提示"
          description="确认删除此用户吗?"
          onConfirm={() => {
            handlerRemoveUser(record.userId);
            actionRef.current?.reload();
            setShowDetail(false);
          }}
          okText="Yes"
          cancelText="No"
        >
          <a>删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <PageContainer>
      <ProTable<RES.UserListItem, REQ.PageParams>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="userId"
        search={{
          labelWidth: 120,
        }}
        dateFormatter={(v) => {
          return v.format('YYYY-MM-DD HH:mm:ss');
        }}
        beforeSearchSubmit={(v) => {
          const { time = [] } = v;
          const payload = {
            ...v,
            start: time[0],
            end: time[1],
          };
          delete payload.time;
          return { ...payload };
        }}
        toolBarRender={() => [
          <BetaSchemaForm<REQ.AddUserParams>
            modalProps={{
              destroyOnClose: true,
              title: '新建用户',
              width: '550px',
            }}
            layout="horizontal"
            labelCol={{ span: 4 }}
            key="primary"
            trigger={
              <Button type="primary">
                <PlusOutlined />
                <FormattedMessage id="pages.searchTable.new" defaultMessage="New" />
              </Button>
            }
            layoutType="ModalForm"
            steps={[
              {
                title: 'ProComponent',
              },
            ]}
            onFinish={async (values) => {
              const res = await handleAddUser(values);
              actionRef.current?.reload();
              return res;
            }}
            columns={columnsForm}
          />,
        ]}
        request={getUserList}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar extra={<div>提示选择了</div>}>
          <Popconfirm
            key="removeUser"
            title="删除提示"
            description="确认删除用户吗?"
            onConfirm={async () => {
              try {
                await handleRemoveAll(selectedRowsState);
                setSelectedRows([]);
                actionRef.current?.reloadAndRest?.();
              } catch (error) {
                console.log(error);
              }
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button>
              <FormattedMessage
                id="pages.searchTable.batchDeletion"
                defaultMessage="Batch deletion"
              />
            </Button>
          </Popconfirm>
        </FooterToolbar>
      )}

      <Drawer
        width={600}
        open={showDetail}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {currentRow?.nickname && (
          <ProDescriptions<RES.UserListItem>
            column={1}
            title={currentRow?.nickname}
            request={async () => ({
              data: currentRow || {},
            })}
            params={{
              userId: currentRow?.userId,
            }}
            columns={columns as ProDescriptionsItemProps<RES.UserListItem>[]}
            editable={{
              onSave: async (pathname, newUser, oldUser) => {
                try {
                  await handlerUpdateUser(pathname as keyof RES.UserListItem, newUser, oldUser);
                  actionRef.current?.reload();
                } catch (error) {
                  console.log(error);
                }
              },
            }}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

export default TableList;
