module.exports = {
  extends: require.resolve('@umijs/max/eslint'),
  globals: {
    page: true,
  },
  rules: {
    'no-multiple-empty-lines': 2,
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-constant-condition': 2,
    'no-trailing-spaces': 1,
    'no-var': 2,
    'consistent-this': [2, 'that'],
    'no-dupe-args': [2],
    'no-empty-pattern': 0,
    'vue/multi-word-component-names': 0,
    'no-empty': 0,
    '@typescript-eslint/no-unused-vars': 'warn',
  },
};
