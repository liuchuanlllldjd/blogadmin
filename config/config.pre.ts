import { defineConfig } from '@umijs/max';

export default defineConfig({
  define: {
    API_URL: 'http://liosummer.cn:3701',
    API_PREFIX: '/api',
  },
});
