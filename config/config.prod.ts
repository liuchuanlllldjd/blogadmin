import { defineConfig } from '@umijs/max';

export default defineConfig({
  define: {
    API_URL: 'http://lioblog.cn',
    API_PREFIX: '/api',
  },
});
